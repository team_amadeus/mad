from mad import *
import time
import os
import subprocess

class MariaProvide(Component):
	def create(self):
		self.places = [
			'waiting',
			'provisioned',
			'bootstrapped',
			'started',
			'checked'
		]

		self.transitions = {
			'provision': ('waiting', 'provisioned', self.provision),
			'pull': ('provisioned', 'bootstrapped', self.pull),
			'bootstrap': ('provisioned', 'bootstrapped', self.bootstrap),
			'conf': ('provisioned', 'bootstrapped', self.conf),
			'start': ('bootstrapped', 'started', self.start),
			'check': ('started', 'checked', self.check)
		}

		self.dependencies = {
			'ip': (DepType.DATA_PROVIDE, ['provisioned']),
			'service': (DepType.PROVIDE, ['checked'])
		}

	def provision(self):
		os.chdir('/home/kyle/Documents/mad/amadeus_examples/mydbcontainer/')

		self.write('ip', "192.168.1.1")

	def pull(self):
		subprocess.call(['docker', 'pull', 'centos'])

	def bootstrap(self):
		time.sleep(5)

	def conf(self):
		time.sleep(5)

	def start(self):
		subprocess.call(['docker', 'build', '-t', 'dbforweb', '.'])
		subprocess.call(['docker', 'run', '-d', '-p', '3306:3306', '--name=mydbforweb', 'dbforweb'])

	def check(self):
		time.sleep(10)
		subprocess.call(['nc', '-v', '172.17.0.1', '3306'])
