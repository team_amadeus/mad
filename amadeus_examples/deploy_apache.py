from mad import *
import time
import os
import subprocess

class ApacheRead(Component):
	def create(self):
		self.places = [
			'waiting',
			'provisioned',
			'bootstrapped',
			'started',
			'checked'
		]

		self.transitions = {
			'provision': ('waiting', 'provisioned', self.prov),
			'bootstrap': ('provisioned', 'bootstrapped', self.config),
			'pull': ('provisioned', 'bootstrapped', self.pull),
			'conf': ('provisioned', 'bootstrapped', self.conf),
			'start': ('bootstrapped', 'started', self.build),
			'check': ('started', 'checked', self.check)
		}

		self.dependencies = {
			'ipprov': (DepType.DATA_USE, ['bootstrap']),
			'service': (DepType.USE, ['check'])
		}

	def prov(self):
		os.chdir('/home/kyle/Documents/mad/amadeus_examples/mywebcontainer/')

	def config(self):
		ip = self.read('ipprov')
		print("Received IP: " + ip)

	def pull(self):
		subprocess.call(['docker', 'pull', 'centos'])

	def conf(self):
		time.sleep(5)

	def build(self):
		#Build then run the container
		os.chdir('/home/kyle/Documents/mad/amadeus_examples/mywebcontainer/')
		subprocess.call(['docker', 'build', '-t', 'webwithdb', '.'])
		subprocess.call(['docker', 'run', '-d', '-p', '80:80', '--name=mywebwithdb', 'webwithdb'])

	def check(self):
		subprocess.call(['curl', 'http://localhost/index.html'])
		subprocess.call(['curl', 'http://localhost/cgi-bin/action'])
