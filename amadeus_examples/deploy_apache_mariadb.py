from mad import *

from deploy_mariadb import MariaProvide
from deploy_apache import ApacheRead

if __name__ == '__main__':
	#Component MariaDB
	maria = MariaProvide()

	#Component Apache
	apache = ApacheRead()

	assembly = Assembly()
	assembly.addComponent('apache', apache)
	assembly.addComponent('maria', maria)
	assembly.addConnection(apache, 'ipprov', maria, 'ip')
	assembly.addConnection(apache, 'service', maria, 'service')

	mad = Mad(assembly)
	mad.run()
